var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	if (uporabniskoIme == "" || geslo == "") {
		res.send({status: "false", napaka: "Napačna zahteva."});
	} else {
		if (preveriSpomin(uporabniskoIme, geslo) ||	preveriDatotekaStreznik(uporabniskoIme, geslo)) {
			res.send({status: "true", napaka: ""});
		} else {
			res.send({status: "false", napaka: "Avtentikacija ni uspela."});
		}
	}
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	if (preveriSpomin(uporabniskoIme, geslo) ||	preveriDatotekaStreznik(uporabniskoIme, geslo)) {
		res.send("<html><title>Uspešno</title><body><p>Uporabnik <b>" + uporabniskoIme + "</b> uspešno prijavljen v sistem!</p></body></html>");
	} else {
		res.send("<html><title>Napaka</title><body><p>Uporabnik <b>" + uporabniskoIme + "</b> nima pravice prijave v sistem!</p></body></html>");
	}
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


//var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var dataString = fs.readFileSync("public/podatki/uporabniki_streznik.json").toString();
var podatkiDatotekaStreznik = JSON.parse(dataString);


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	var uporabnik = uporabniskoIme + "/" + geslo;
	for (var i in podatkiSpomin) {
		if (podatkiSpomin[i] == uporabnik) {
			return true;
		}
	}
	return false;
	// ...
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (var i in podatkiDatotekaStreznik) {
		if (podatkiDatotekaStreznik[i].uporabnik == uporabniskoIme && podatkiDatotekaStreznik[i].geslo == geslo) {
			return true;
		}
	}
	return false;
	// ...
}